%
% pca_reduce.m
%
% Use principal component analysis to reduce the dimensionality of the given
% NxD dataset. Where N is number of samples and D is the dimensionality (features)
% to a M dimensional dataset where M < D
%
% parameters:
%   data_m - matrix - dataset (not including class) to reduce
%   thresh - scalar - target variance
%
% returns:
%   d - matrix  - a NxM reduced dataset
%   pc- matrix  - the principal components
%   v - scalar  - percent variance captured in reduction
%   M - scalar  - number of dimensions in the new dataset

function [ d, pc, v, M ] = pca_reduce( data_m, thresh)
  %data_m = center(data_m); %subtract the mean of each column from each element of the column
  
  %unit variance 
  %data_m = data_m*diag(1./std(data_m));

  cov_m = cov(data_m);
      
  [u, eig_vals, pc] = svd(cov_m);
    
  eig_vals = diag(eig_vals);
  %select the number of components needed to meet variance threshold
  M = 0;
  t = 0;
  
  while( t < thresh )
    M = M + 1;
    t = sum( eig_vals(1:M) ) / sum(eig_vals);
  end
  
  pc = pc(:, 1:M);

  d = data_m*pc;
  if nargout > 1
    v = sum( eig_vals(1:M) ) / sum(eig_vals);
  end

end
