%
%   polyextend.m
%   
%   given a dataset with variables x1 x2 x3 ... xN create a new set of
%   variables x1^1 x1^2 ... x1^p for each of the original variables
%   
%   the new dataset will have X * P variables
%   
%   arguments
%       data_m  - dataset to extend (excluding final truth column)
%       p       - max polynomial degree
%
%   returns
%       extend_v - the dataset with extended polynomial values
%


function [ extend_m ] = polyextend( data_m, P )
  
    N = size(data_m, 2);
    M = size(data_m, 1);

    extend_m = zeros(M, N * P);
  
    for n=1:N
      for p=1:P
        extend_m(:, (n-1)*P + p) = data_m(:, n).^p;
      end
    end

end

