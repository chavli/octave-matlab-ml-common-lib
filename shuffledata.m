%
% shuffledata.m
%
% shuffles the samples(rows) in the given dataset
%
% arguments:
%   data_m  - the dataset to be shuffled
%

function [ sdata_m ] = shuffledata( data_m )
    num_samples = size(data_m, 1);
    order = randperm(num_samples);
    sdata_m = data_m(order, :);
end

