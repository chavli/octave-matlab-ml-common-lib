%
% mape.m
% 
% calculates the mean absolute percent error (MAPE) given the true time-series
% data and the predicted time-series data.
%
% arguments
%   actual_v  - true dataseries
%   guess_v   - predicted dataseries
%
% returns
%   err - the mape value in the range [0, 1]

function [ err ] = mape(actual_v, guess_v)
  N = length(actual_v);

  err = sum(abs((actual_v - guess_v) ./ actual_v )) / N; 
end
