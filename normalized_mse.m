%
% normalized_mse.m
%
% calculates the normalized mean square error of the given predictions.
% all data is normalized to the true values meaning the true values are
% represented by a vector of 1's and the predicted values are either 
% greater of less than 1.
%
% nomalized mse is intended to compare the performance of two models independent
% of the magnitude of the values.
%
% arguments:
%   predict_v - vector of predictions
%   actual_v  - vector of true values
%
% returns
%   nmse    - normalized mean square error
%   norm_p  - normalized prediction vector
%   norm_a  - normalized truth vector
%

function [nmse, norm_p, norm_a] = normalized_mse(predict_v, actual_v)
  nmse = 0;
  norm_a = actual_v ./ actual_v;
  norm_p = predict_v ./ actual_v;

  nmse = mean_square_error(norm_p, norm_a);

end





