%
% linfit.m
%
% calculates the weights for a multivariate linear regression model given a set
% of training data. a univariate model can be fitted by using a dataset with
% one input variable.
%
% this code implements online gradient descent and the normal equation 
% (both regularized) for calculating the weight vector theta. online GD 
% is the default method used but the normal equation can be specified
% as an argument.
%
%
% some notes:
%   -MSE is used as the error function
%   -the dataset must be invertible for the normal equation
%   -the normal equation will only be used on datasets with < 1000 variables
%
%
% arguments:
%   data_m  - a dataset with m samples and n variables including a final
%     "truth" column
%   iters   - the number of iterations used by gradient descent
%   lambda  - regularization constant (default = 0 (no regularization))
%   mode    - gradient descent or normal equation (default = 0 (GD))
% 
% returns:
%   theta - a vector of weights to be used by multivariate linear regression
%     model (includes the bias term).
%

function [theta] = linfit(data_m, iters, lambda = 0, mode = 0)

  M = size(data_m, 1);
  
  %add bias
  data_m = [ones(M, 1), data_m];

  N = size(data_m, 2) - 1;
  
  X = data_m(:, 1:N);
  y = data_m(:, end);

  %random initial weights + bias
  theta = rand(1, N);
   
  if N < 1000 && mode == 1
    %regularized normal equation: inv(X'X) * X'y
    theta = pinv(X' * X + lambda * eye(N, N)) * (X' * y);
  else
    %regularized online gradient descent
    for i=1:iters
      %learning rate
      alpha = 1 / (i + 1);
      
      x = X(i, :);
      truth = y(i);
      guess = theta * x';

      theta = theta - alpha * ((guess - truth)*x + (lambda) * theta);
    end
  end
end



