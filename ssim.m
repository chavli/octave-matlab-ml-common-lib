%
% ssim.m
%
% calculates the structural similarity index measure between two signals.
% 
% arguments
%   actual_v  - the actual timeseries of data
%   guess_v   - the predicted timeseries of data
%
% returns 
%   simi  - the ssim value
%

function [ simi ] = ssim( actual_v, guess_v )
  if ~iscolumn(actual_v)
    actual_v = actual_v';
  end

  if ~iscolumn(guess_v)
    guess_v = guess_v';
  end
  
  actual_v = actual_v ./ max(actual_v);
  guess_v = guess_v ./ max(guess_v);

  L = max(log(max(actual_v)), log(max(guess_v)));
  c1 = (0.01 * L)^2;
  c2 = (0.03 * L)^2;

  mu_a = mean(actual_v); 
  mu_g = mean(guess_v);  

  cov_ag = cov([actual_v, guess_v]);
  
  simi = ((2 * mu_g * mu_a + c1) * (2 * cov_ag(2, 1) + c2)) /...
    ((mu_g^2 + mu_a^2 + c1) * (cov_ag(1, 1) + cov_ag(2, 2) + c2));
end


