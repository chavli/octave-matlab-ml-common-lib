%
% linreg.m
%
% performs regression given the weights of the model and a dataset of samples.
% this is a vectorized implemenation of linear regression so it can be  
% univariate or multivariate based on the given weights.
%
% arguments:
%   data_m  - a dataset with m samples and n variables including a final 
%     "truth" column.
%   theta_v - a vector, size n+1, of weights which includes the bias value
%
% returns:
%   guess_v   - a vector of the predictions for each given sample
%   err       - the total err (MAPE)
%   avg_time  - avg cpu time required to predict
%

function [guess_v, err, avg_time] = linreg(data_m, theta_v, norm=false)

  M = size(data_m, 1);

  %add bias term
  data_m = [ones(M, 1), data_m];
  guess_v = zeros(M, 1);
  
  if ~isrow(theta_v)
    theta_v = theta_v';
  end
  
  t = cputime;
  for i=1:M
    guess_v(i, 1) = theta_v * data_m(i, 1:end-1)';
  end
  avg_time = (cputime - t)/M;

  err = mape(data_m(:, end), guess_v);

end


