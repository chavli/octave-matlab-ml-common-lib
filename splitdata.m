%   
% splitdata.m
%   
% splits a given dataset into two disjoint datasets (training and testing).
% The final split is determined by a percentage p [0-1] where (1 - p)
% samples will be placed into the training set with the remaining p samples
% in the test set.
%
%   arguments:
%       data_m - a matrix representing the whole dataset
%       p - percentage [0-1] of samples that will go into the test dataset
%   
%   returns:
%       testing_m - a subset of data_m used for testing an agent
%       training_m - a subset of data_m used for training an agent
%

function [ training_m, testing_m ] = splitdata( data_m, p )
    max_training = ceil( (1 - p) * size(data_m, 1));
    max_testing = floor( p * size(data_m, 1));

    cols = size(data_m, 2);
    training_m = zeros(max_training, cols);
    testing_m = zeros(max_testing, cols);
    ctr_0 = 1; ctr_1= 1;
    
    rand('state', 'reset')'

    for i = 1:length(data_m)
        if(( rand(1) < p && max_testing > 0) || max_training == 0)
            testing_m(ctr_0, :) = data_m(i, :);
            ctr_0 = ctr_0 + 1;
            max_testing = max_testing - 1;
        else
            training_m(ctr_1, :) = data_m(i, :);
            ctr_1 = ctr_1 + 1;
            max_training = max_training - 1;
        end
    end
    
end

